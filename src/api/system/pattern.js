import request from '@/utils/request'

// 查询设备控制模式列表
export function listPattern(query) {
  return request({
    url: '/system/pattern/list',
    method: 'get',
    params: query
  })
}
// 查询设备控制模式列表
export function listPatterns(query) {
  return request({
    url: '/system/pattern/listAll',
    method: 'get',
    params: query
  })
}

// 查询设备控制模式详细
export function getPattern(id) {
  return request({
    url: '/system/pattern/' + id,
    method: 'get'
  })
}

// 新增设备控制模式
export function addPattern(data) {
  return request({
    url: '/system/pattern',
    method: 'post',
    data: data
  })
}

// 修改设备控制模式
export function updatePattern(data) {
  return request({
    url: '/system/pattern/update',
    method: 'post',
    data: data
  })
}

// 删除设备控制模式
export function delPattern(id) {
  return request({
    url: '/system/pattern/del/' + id,
    method: 'post'
  })
}
