import request from '@/utils/request'

// 查询设备信息列表
export function listInfo(query) {
  return request({
    url: '/system/info/list',
    method: 'get',
    params: query
  })
}

// 查询设备信息详细
export function getInfo(id) {
  return request({
    url: '/system/info/' + id,
    method: 'get'
  })
}

// 新增设备信息
export function addInfo(data) {
  return request({
    url: '/system/info',
    method: 'post',
    data: data
  })
}

// 修改设备信息
export function updateInfo(data) {
  return request({
    url: '/system/info/edit',
    method: 'post',
    data: data
  })
}

// 删除设备信息
export function delInfo(id) {
  return request({
    url: '/system/info/' + id,
    method: 'post'
  })
}
// 部门 信息
export function getListTree(id) {
  return request({
    url: '/system/dept/listTree',
    method: 'get'
  })
}
// 设备控制修改
export function setEqChange(data) {
  return request({
    url: '/system/control',
    method: 'post',
    data: data
  })
}
// 设备开关机
export function controlOpen(data) {
  return request({
    url: '/system/control/controlOpen',
    method: 'post',
    data: data
  })
}
// 定时开关机
export function controlTimingOpen(data) {
  return request({
    url: '/system/control/controlTimingOpen',
    method: 'post',
    data: data
  })
}
