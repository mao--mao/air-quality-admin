import request from '@/utils/request'

// 查询应用版本列表
export function listVersion (query) {
  return request({
    url: '/system/version/list',
    method: 'get',
    params: query
  })
}

// 查询应用版本详细
export function getVersion (id) {
  return request({
    url: '/system/version/' + id,
    method: 'get'
  })
}

// 新增应用版本
export function addVersion (data) {
  return request({
    url: '/system/version',
    method: 'post',
    data: data
  })
}

// 修改应用版本
export function updateVersion (data) {
  return request({
    url: '/system/version/edit',
    method: 'post',
    data: data
  })
}

// 删除应用版本
export function delVersion (id) {
  return request({
    url: '/system/version/' + id,
    method: 'delete'
  })
}
