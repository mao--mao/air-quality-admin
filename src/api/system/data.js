import request from '@/utils/request'

// 查询设备故障信息列表
export function listData(query) {
  return request({
    url: '/system/malfunctionData/list',
    method: 'get',
    params: query
  })
}

// 查询设备故障信息详细
export function getData(id) {
  return request({
    url: '/system/malfunctionData/' + id,
    method: 'get'
  })
}

// 新增设备故障信息
export function addData(data) {
  return request({
    url: '/system/malfunctionData',
    method: 'post',
    data: data
  })
}

// 修改设备故障信息
export function updateData(data) {
  return request({
    url: '/system/malfunctionData/edit',
    method: 'post',
    data: data
  })
}

// 删除设备故障信息
export function delData(id) {
  return request({
    url: '/system/malfunctionData/del/' + id,
    method: 'post'
  })
}
