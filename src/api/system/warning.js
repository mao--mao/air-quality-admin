import request from '@/utils/request'

// 查询设备报警信息列表
export function listWarning(query) {
  return request({
    url: '/system/warning/list',
    method: 'get',
    params: query
  })
}

// 查询设备报警信息详细
export function getWarning(id) {
  return request({
    url: '/system/warning/' + id,
    method: 'get'
  })
}

// 新增设备报警信息
export function addWarning(data) {
  return request({
    url: '/system/warning',
    method: 'post',
    data: data
  })
}

// 修改设备报警信息
export function updateWarning(data) {
  return request({
    url: '/system/warning',
    method: 'put',
    data: data
  })
}

// 删除设备报警信息
export function delWarning(id) {
  return request({
    url: '/system/warning/' + id,
    method: 'delete'
  })
}
