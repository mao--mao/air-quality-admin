import request from '@/utils/request'

// 设备状态占比
export function getStatusApi () {
  return request({
    url: '/system/info/listCount',
    method: 'get'
  })
}
// 设备平均杀菌率
export function getAirAreaApi () {
  return request({
    url: '/system/info/airAll',
    method: 'get'
  })
}
// 报警列表
export function getWarningInfoApi (query) {
  return request({
    url: '/system/warning/list',
    method: 'get',
    params:query
  })
}
